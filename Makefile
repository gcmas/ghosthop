EXE=ghosthop
TCC=tcc
WINCC=x86_64-w64-mingw32-gcc

.PHONY: all repl bindings init linux windows wasm clean
all: repl bindings init linux windows wasm
repl: build/repl
bindings: build/so/librlbind.so
init: build/$(EXE)
linux: build/linux/$(EXE)-linux.tar.gz
windows: build/windows/$(EXE)-windows.tar.gz
wasm: build/wasm/$(EXE)-wasm.tar.gz
clean:
	rm -rf build

build/repl: src/repl.c build/so/libs7.so build/so/libraylib.so
	$(TCC) -g -Ivendor/s7 -o $@ $^ -Lbuild/so -Wl,-rpath,./build/so -lm -ldl -ls7 -lraylib

build/so/libs7.so: vendor/s7/s7.c
	mkdir -p build build/so
	touch build/mus-config.h
	$(TCC) -g -Ibuild -o $@ $^ -fPIC -shared -lm -ldl

build/so/libraylib.so: vendor/raylib/src
	mkdir -p build/so build/raylib
	rm -f $(wildcard build/raylib/*)
	cd build/raylib && ln -s $(wildcard $(PWD)/$^/*) .
	cd build/raylib && $(MAKE) PLATFORM=PLATFORM_DESKTOP RAYLIB_LIBTYPE=SHARED RAYLIB_SRC_PATH=.
	mv build/raylib/libraylib.so* build/so

build/so/librlbind.so: build/rlbind.c
	mkdir -p build/so
	$(TCC) -g -Ivendor/s7 -Ivendor/raylib/src -o $@ $^ -fPIC -shared \
		-Lbuild/so -Wl,-rpath,./build/so -lm -ls7 -lraylib

build/rlbind.c: build/repl rlbind.scm
	mkdir -p build
	./build/repl rlbind.scm > $@

build/$(EXE): src/init.c build/so/libs7.so build/so/libraylib.so
	$(TCC) -g -Ivendor/s7 -Ivendor/raylib/src -DHOT_RELOAD -o $@ $^ \
		-Lbuild/so -Wl,-rpath,./build/so -lm -ldl -ls7 -lraylib

build/linux/raylib/libraylib.a: vendor/raylib/src
	mkdir -p build/linux/raylib
	rm -f $(wildcard build/linux/raylib/*)
	cd build/linux/raylib && ln -s $(wildcard $(PWD)/$^/*) .
	cd build/linux/raylib && $(MAKE) PLATFORM=PLATFORM_DESKTOP RAYLIB_SRC_PATH=.

build/linux/s7.o: vendor/s7/s7.c
	mkdir -p build/linux
	touch build/mus-config.h
	$(CC) -O3 -Ibuild -c -o $@ $^

build/linux/$(EXE): src/init.c build/rlbind.c build/linux/raylib/libraylib.a build/linux/s7.o
	$(CC) -O3 -Ivendor/s7 -Ivendor/raylib/src -o $@ $^ -lm

build/linux/$(EXE)-linux.tar.gz: asset scm build/linux/$(EXE)
	tar -czf $@ asset scm --directory=build/linux $(EXE) --transform 's,^,$(EXE)-linux/,'

build/windows/raylib/libraylib.a: vendor/raylib/src
	mkdir -p build/windows/raylib
	rm -f $(wildcard build/windows/raylib/*)
	cd build/windows/raylib && ln -s $(wildcard $(PWD)/$^/*) .
	cd build/windows/raylib \
		&& $(MAKE) CC=$(WINCC) PLATFORM=PLATFORM_DESKTOP PLATFORM_OS=WINDOWS PLATFORM_HOST=LINUX \
			RAYLIB_SRC_PATH=.

build/windows/s7.o: vendor/s7/s7.c
	mkdir -p build/windows
	touch build/mus-config.h
	$(WINCC) -O3 -Ibuild -c -o $@ $^

build/windows/$(EXE).exe: src/init.c build/rlbind.c build/windows/raylib/libraylib.a build/windows/s7.o
	$(WINCC) -O3 -Ivendor/s7 -Ivendor/raylib/src -o $@ $^ \
		-lopengl32 -lgdi32 -lwinmm -lpthread -static

build/windows/$(EXE)-windows.tar.gz: asset scm build/windows/$(EXE).exe
	tar -czf $@ asset scm --directory=build/windows $(EXE).exe --transform 's,^,$(EXE)-windows/,'

build/wasm/raylib/libraylib.a: vendor/raylib/src
	mkdir -p build/wasm/raylib
	rm -f $(wildcard build/wasm/raylib/*)
	cd build/wasm/raylib && ln -s $(wildcard $(PWD)/$^/*) .
	cd build/wasm/raylib && $(MAKE) PLATFORM=PLATFORM_WEB RAYLIB_SRC_PATH=.

build/wasm/s7.o: vendor/s7/s7.c
	mkdir -p build/wasm
	touch build/mus-config.h
	emcc -Oz -Ibuild -c -o $@ $^

build/wasm/$(EXE)-wasm: asset scm src/init.c build/rlbind.c build/wasm/raylib/libraylib.a build/wasm/s7.o
	mkdir -p build/wasm/$(EXE)-wasm
	emcc -Oz -DPLATFORM_WEB -Ivendor/s7 -Ivendor/raylib/src \
		-sUSE_GLFW=3 --shell-file vendor/raylib/src/minshell.html --preload-file asset --preload-file scm \
		-o $@/index.html src/init.c build/rlbind.c build/wasm/raylib/libraylib.a build/wasm/s7.o \
		-lm

build/wasm/$(EXE)-wasm.tar.gz: build/wasm/$(EXE)-wasm
	tar -czf $@ --directory build/wasm $(EXE)-wasm

