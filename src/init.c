#if defined(HOT_RELOAD)
# define _GNU_SOURCE
# include <dlfcn.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "raylib.h"
#include "s7.h"

#if defined(PLATFORM_WEB)
# include "emscripten.h"
#endif

#define LIBRLBIND_PATH "./build/so/librlbind.so"

struct context {
  s7_scheme *sc;
  s7_pointer env_bind;
  s7_pointer env_load;
  s7_pointer sym_init;
  s7_pointer sym_fini;
  s7_pointer sym_take;
  s7_pointer sym_pass;
  s7_pointer sym_done;
  s7_pointer sym_step;
  s7_pointer sym_wasm;
  s7_pointer sym_reload_dynlib;
  s7_pointer sym_reload_script;
  int is_done;

# if defined(HOT_RELOAD)
  void *dl_handle;
  void (*rlbind)(s7_scheme *sc);
# endif
};

#if !defined(HOT_RELOAD)
void rlbind(s7_scheme *sc);
#endif

s7_pointer try_call_symbol(s7_scheme *sc, s7_pointer sym) {
  s7_pointer curlet = s7_curlet(sc);
  s7_pointer proc = s7_let_ref(sc, curlet, sym);
  if (!s7_is_procedure(proc)) return s7_f(sc);
  return s7_call(sc, proc, s7_nil(sc));
}

s7_pointer set_outlet(s7_scheme *sc, s7_pointer env, s7_pointer new_outlet) {
  s7_pointer old_outlet = s7_outlet(sc, env);
  s7_pointer outlet_setter = s7_setter(sc,
      s7_let_ref(sc, s7_rootlet(sc), s7_make_symbol(sc, "outlet")));
  s7_call(sc, outlet_setter, s7_list(sc, 2, env, new_outlet));
  return old_outlet;
}

#if defined(HOT_RELOAD)
int load_rlbind(struct context *ctx) {
  void *handle;
  void (*rlbind_proc)(s7_scheme *sc);

  if (ctx->dl_handle != NULL) {
    dlclose(ctx->dl_handle);
    ctx->dl_handle = NULL;
    *(void **) &ctx->rlbind = NULL;
  }

  handle = dlopen(LIBRLBIND_PATH, RTLD_NOW);
  if (handle == NULL) {
    fprintf(stderr, "dlopen: %s\n", dlerror());
    return -1;
  }

  *(void **) &rlbind_proc = dlsym(handle, "rlbind");
  if (rlbind_proc == NULL) {
    fprintf(stderr, "dlsym: %s\n", dlerror());
    return -1;
  }

  ctx->dl_handle = handle;
  ctx->rlbind = rlbind_proc;

  return 0;
}
#endif

void load_script(struct context *ctx) {
  s7_load_with_environment(ctx->sc, "scm/init.scm", ctx->env_load);
}

void main_loop(void *ctx_erased) {
  struct context *ctx = ctx_erased;

  ctx->is_done = s7_boolean(ctx->sc, try_call_symbol(ctx->sc, ctx->sym_done));
  if (ctx->is_done) {
#   if defined(PLATFORM_WEB)
    emscripten_cancel_main_loop();
#   endif
    return;
  }

  try_call_symbol(ctx->sc, ctx->sym_step);

# if defined(HOT_RELOAD)
  if (IsKeyPressed(KEY_F5) || s7_boolean(ctx->sc, s7_let_ref(ctx->sc, ctx->env_load, ctx->sym_reload_dynlib))) {
    ctx->env_bind = s7_sublet(ctx->sc, s7_rootlet(ctx->sc), s7_nil(ctx->sc));
    set_outlet(ctx->sc, ctx->env_load, ctx->env_bind);
    s7_set_curlet(ctx->sc, ctx->env_bind);
    if (load_rlbind(ctx) == 0) ctx->rlbind(ctx->sc);

    try_call_symbol(ctx->sc, ctx->sym_pass);
    load_script(ctx);
    try_call_symbol(ctx->sc, ctx->sym_take);
  }
  if (IsKeyPressed(KEY_F6) || s7_boolean(ctx->sc, s7_let_ref(ctx->sc, ctx->env_load, ctx->sym_reload_script))) {
    try_call_symbol(ctx->sc, ctx->sym_pass);
    load_script(ctx);
    try_call_symbol(ctx->sc, ctx->sym_take);
  }
# endif

  s7_let_set(ctx->sc, ctx->env_load, ctx->sym_reload_dynlib, s7_make_boolean(ctx->sc, 0));
  s7_let_set(ctx->sc, ctx->env_load, ctx->sym_reload_script, s7_make_boolean(ctx->sc, 0));
}

int main(int argc, char *argv[]) {
  struct context ctx = { 0 };

  (void) argc;
  (void) argv;

  ctx.sc = s7_init();
  ctx.env_bind = s7_nil(ctx.sc);
  ctx.env_load = s7_nil(ctx.sc);
  ctx.sym_init = s7_make_symbol(ctx.sc, "init");
  ctx.sym_fini = s7_make_symbol(ctx.sc, "fini");
  ctx.sym_take = s7_make_symbol(ctx.sc, "take");
  ctx.sym_pass = s7_make_symbol(ctx.sc, "pass");
  ctx.sym_done = s7_make_symbol(ctx.sc, "done?");
  ctx.sym_step = s7_make_symbol(ctx.sc, "step");
  ctx.sym_wasm = s7_make_symbol(ctx.sc, "wasm?");
  ctx.sym_reload_dynlib = s7_make_symbol(ctx.sc, "reload-dynlib?");
  ctx.sym_reload_script = s7_make_symbol(ctx.sc, "reload-script?");
  ctx.is_done = 0;
# if defined(HOT_RELOAD)
  ctx.dl_handle = NULL;
  ctx.rlbind = NULL;
# endif

  s7_gc_protect(ctx.sc, ctx.sym_init);
  s7_gc_protect(ctx.sc, ctx.sym_fini);
  s7_gc_protect(ctx.sc, ctx.sym_take);
  s7_gc_protect(ctx.sc, ctx.sym_pass);
  s7_gc_protect(ctx.sc, ctx.sym_done);
  s7_gc_protect(ctx.sc, ctx.sym_step);

  ctx.env_bind = s7_sublet(ctx.sc, s7_curlet(ctx.sc), s7_nil(ctx.sc));
  ctx.env_load = s7_sublet(ctx.sc, ctx.env_bind, s7_nil(ctx.sc));
  s7_set_curlet(ctx.sc, ctx.env_bind);
# if defined(HOT_RELOAD)
  if (load_rlbind(&ctx) != 0) {
    return EXIT_FAILURE;
  }
  ctx.rlbind(ctx.sc);
# else
  rlbind(ctx.sc);
# endif

# if defined(PLATFORM_WEB)
  s7_define(ctx.sc, ctx.env_load, ctx.sym_wasm, s7_make_boolean(ctx.sc, 1));
# else
  s7_define(ctx.sc, ctx.env_load, ctx.sym_wasm, s7_make_boolean(ctx.sc, 0));
# endif
  s7_define(ctx.sc, ctx.env_load, ctx.sym_reload_dynlib, s7_make_boolean(ctx.sc, 0));
  s7_define(ctx.sc, ctx.env_load, ctx.sym_reload_script, s7_make_boolean(ctx.sc, 0));

  load_script(&ctx);

  try_call_symbol(ctx.sc, ctx.sym_init);

#if defined(PLATFORM_WEB)
  emscripten_set_main_loop_arg(main_loop, &ctx, 0, 1);
#else
  do main_loop(&ctx); while (!ctx.is_done);
#endif

  try_call_symbol(ctx.sc, ctx.sym_fini);

  s7_free(ctx.sc);

  return EXIT_SUCCESS;
}

