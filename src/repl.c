#include <dlfcn.h>
#include <stdlib.h>
#include <unistd.h>

#include "s7.h"

#define LIBRLBIND_PATH "./build/so/librlbind.so"

void *dl_handle = NULL;

static s7_pointer update_bindings(s7_scheme *sc, s7_pointer args) {
  void (*rlbind_proc)(s7_scheme *sc) = NULL;

  (void) args;

  if (dl_handle != NULL) {
    dlclose(dl_handle);
    dl_handle = NULL;
  }

  dl_handle = dlopen(LIBRLBIND_PATH, RTLD_NOW);
  if (dl_handle == NULL) {
    s7_pointer error_str = s7_make_string(sc, dlerror());
    return s7_error(sc, s7_make_symbol(sc, "update-bindings"), s7_cons(sc, error_str, s7_nil(sc)));
  }

  *(void **) &rlbind_proc = dlsym(dl_handle, "rlbind");
  if (rlbind_proc == NULL) {
    s7_pointer error_str = s7_make_string(sc, dlerror());
    return s7_error(sc, s7_make_symbol(sc, "update-bindings"), s7_cons(sc, error_str, s7_nil(sc)));
  }

  rlbind_proc(sc);
  return s7_unspecified(sc);
}

int main(int argc, char *argv[]) {
  s7_scheme *sc = s7_init();
  s7_define_typed_function(sc, "update-bindings", update_bindings, 0, 0, 0,
    "(update-bindings) dynamically loads the shared object " LIBRLBIND_PATH " and calls `rlbind`",
    s7_make_signature(sc, 1, s7_make_symbol(sc, "unspecified?")));
  if (argc <= 1) {
    s7_repl(sc);
  } else {
    int i;
    for (i = 1; i < argc; ++i) s7_load(sc, argv[i]);
  }
  s7_free(sc);
  return EXIT_SUCCESS;
}
