(load "scm/levels.scm")

(define defvar
  (macro (name value)
    `(',unless (',defined? ',name)
       (',define ,name ,value))))

(define assert
  (macro (tag predicate-expr)
    `(',unless ,predicate-expr
       (',error ,tag "Assertion failed: ~a\n" ',predicate-expr))))

(define (list-head items i)
  (let loop ((items items)
             (rlis '())
             (i i))
    (cond ((zero? i) (reverse rlis))
          ((null? items) (error 'list-head "Not enough items in list"))
          (else (loop (cdr items) (cons (car items) rlis) (- i 1))))))

(define (iota n)
  (let loop ((items '())
             (n n))
    (if (zero? n)
        items
        (let ((i (- n 1)))
          (loop (cons i items) i)))))

(define (clamp x min-val max-val)
  (max (min x max-val) min-val))

(defvar font-default #f)

(defvar music #f)
(defvar sound-bump #f)
(defvar sound-color-slide #f)
(defvar sound-jump #f)
(defvar sound-land #f)
(defvar sound-lose #f)
(defvar sound-slide #f)
(defvar sound-win #f)

(defvar texture-cover #f)
(defvar texture-ghost-top #f)
(defvar texture-ghost-flap #f)
(defvar texture-ghost-feet #f)
(defvar texture-icons #f)
(defvar texture-tiles #f)

(defvar image-game-icon #f)

(define sprite-size 32)
(define tile-screen-size 64)

(defvar screen-width #f)
(defvar screen-height #f)
(defvar real-screen-width #f)
(defvar real-screen-height #f)
(defvar letterbox-x #f)
(defvar letterbox-y #f)
(defvar letterbox-width #f)
(defvar letterbox-height #f)

(define (letterbox-update)
  (set! screen-width (max 1 (* total-width tile-screen-size)))
  (set! screen-height (max 1 (* total-height tile-screen-size)))
  (set! real-screen-width (GetScreenWidth))
  (set! real-screen-height (GetScreenHeight))
  (if (< (* real-screen-width screen-height) (* screen-width real-screen-height))
      (begin
        (set! letterbox-width real-screen-width)
        (set! letterbox-height (/ (* real-screen-width screen-height) screen-width)))
      (begin
        (set! letterbox-height real-screen-height)
        (set! letterbox-width (/ (* real-screen-height screen-width) screen-height))))
  (set! letterbox-x (/ (- real-screen-width letterbox-width) 2))
  (set! letterbox-y (/ (- real-screen-height letterbox-height) 2)))
      

(define (letterbox-map/rectangle rect)
  (let ((x (vector-ref rect 0))
        (y (vector-ref rect 1))
        (w (vector-ref rect 2))
        (h (vector-ref rect 3)))
    (vector (+ letterbox-x (/ (* x letterbox-width) screen-width))
            (+ letterbox-y (/ (* y letterbox-height) screen-height))
            (/ (* w letterbox-width) screen-width)
            (/ (* h letterbox-height) screen-height))))

(define (letterbox-map/point point)
  (let ((x (vector-ref point 0))
        (y (vector-ref point 1)))
    (vector (+ letterbox-x (/ (* x letterbox-width) screen-width))
            (+ letterbox-y (/ (* y letterbox-height) screen-height)))))

(define (letterbox-map/font-size font-size)
  (floor (/ (* font-size letterbox-height) screen-height)))

(define ghost-blink-frame-rate 12)
(define ghost-blink-frame-time (/ ghost-blink-frame-rate))
(define ghost-flap-frame-rate 12)
(define ghost-flap-frame-time (/ ghost-flap-frame-rate))
(define ghost-flap-sprite-count 13)
(define ghost-feet-sprite-count 7)

(define ghost-top-sprite-indices
  '((idle 0)
    (squint 1)
    (blink 2)))

(define tile-sprite-indices
  '((wall 0)
    (free 1)
    (lose 2)
    (win 3)))

(define icon-sprite-indices
  '((box 0)
    (? 1)
    (w 2)
    (a 3)
    (s 4)
    (d 5)
    (right 6)
    (up 7)
    (left 8)
    (down 9)
    (touch 10)
    (pause 11)
    (reset 12)
    (rewind 13)
    (play 14)
    (home 15)
    (cancel 16)
    (r 17)
    (e 18)
    (+ 19)
    (- 20)
    (sound 21)
    (settings 22)
    (music 23)
    (dash 24)
    (digit/0 25)))

(define input-delta-xy
  '((up     0 -1)
    (down   0 +1)
    (left  -1  0)
    (right +1  0)))

(define input-keys
  `((up ,KEY_W ,KEY_UP)
    (down ,KEY_S ,KEY_DOWN)
    (left ,KEY_A ,KEY_LEFT)
    (right ,KEY_D ,KEY_RIGHT)
    (confirm ,KEY_ENTER ,KEY_Z)
    (cancel ,KEY_BACKSPACE ,KEY_X)
    (undo ,KEY_U ,KEY_C)
    (reset ,KEY_R ,KEY_V)))

(define input-gestures
  `((up ,GESTURE_SWIPE_UP)
    (down ,GESTURE_SWIPE_DOWN)
    (left ,GESTURE_SWIPE_LEFT)
    (right ,GESTURE_SWIPE_RIGHT)))

(define color-data
  '((red #(255 0 0 255))
    (green #(0 255 0 255))
    (blue #(0 0 255 255))
    (cyan #(0 255 255 255))
    (purple #(255 0 255 255))))

(define game-states
  '((main-menu)
    (selection)
    (level)))

(defvar game-state 'main-menu)
(defvar selection-major 1)
(defvar selection-minor 1)

(defvar levels #f)
(defvar the-level-index #f)
(defvar the-level #f)
(define level-width 7)
(define level-height 7)

(define level-colors (map car color-data))
(defvar level-color-map '())
(defvar undo-stack '())
(define undo-stack-max-count 256)

(defvar player-x 0)
(defvar player-y 3)

(defvar should-exit? #f)

(defvar ready-for-input? #t)
(defvar input #f)
(defvar active-colors '())

(defvar animator #f)

(defvar player-screen-x 0)
(defvar player-screen-y 0)
(defvar player-animating? #f)
(defvar ghost-flap-stage 0)
(defvar ghost-flap-direction +1)
(define ghost-flap-direction-switch-time 5)
(defvar ghost-flap-direction-time 0.0)
(defvar ghost-flap-time 0.0)
(defvar ghost-feet-stage 0)
(defvar ghost-top-stage 'idle)
(defvar ghost-blink-stage 'idle)
(define ghost-blink-interval-min (* 5 ghost-blink-frame-rate))
(define ghost-blink-interval-max (* 10 ghost-blink-frame-rate))
(defvar ghost-blink-interval
  (inexact->exact
   (floor (+ ghost-blink-interval-min
             (random (- ghost-blink-interval-max
                        ghost-blink-interval-min))))))
(define ghost-top-stages
  '((idle idle)
    (squint-in squint)
    (blink blink)
    (squint-out squint)))
(defvar ghost-blink-time 0.0)
(defvar win? #f)
(defvar lose? #f)

(define top-height 1)
(define bottom-height 1)
(define total-width level-width)
(define total-height (+ top-height level-height bottom-height))

(define window-width (* total-width tile-screen-size))
(define window-height (* total-height tile-screen-size))
(define window-title "GHOSTHOP")

(define (make-level number-major number-minor name start-x start-y tiles)
  (assert 'make-level (vector? tiles))
  (assert 'make-level (= (vector-length tiles) (* 7 7)))
  (vector number-major number-minor name start-x start-y tiles #f))

(define (level-major level)
  (vector-ref level 0))
(define (level-minor level)
  (vector-ref level 1))
(define (level-name level)
  (vector-ref level 2))
(define (level-start-x level)
  (vector-ref level 3))
(define (level-start-y level)
  (vector-ref level 4))
(define (level-tiles level)
  (vector-ref level 5))
(define (level-won? level)
  (vector-ref level 6))
(define (level-win! level)
  (vector-set! level 6 #t))

(define (parse-level number-major number-minor name start-x start-y char->tile string-tiles)
  (define default-char->tile
    '((#\. . lose)
      (#\@ . win)
      (#\_ . free)
      (#\# . wall)
      (#\r red)
      (#\g green)
      (#\b blue)
      (#\c cyan)
      (#\p purple)))
  (define tiles
    (list->vector
      (apply append
        (map (lambda (c)
               (if (char-whitespace? c)
                   '()
                   (list
                     (cdr (or (assoc c char->tile)
                              (assoc c default-char->tile))))))
             string-tiles))))
  (make-level number-major number-minor name start-x start-y tiles))

(define (level-load level-index)
  (let ((level (vector-ref levels level-index)))
    (set! the-level-index level-index)
    (set! the-level level)
    (level-state-load (initial-level-state level))
    (set! undo-stack '())))

(define (level-major-minor->index major minor)
  (call-with-exit
    (lambda (return)
      (for-each
        (lambda (index level)
          (when (and (= major (level-major level))
                     (= minor (level-minor level)))
            (return index)))
        (iota (vector-length levels))
        levels)
      (return #f))))

(define (make-level-state player-x player-y active-colors level-color-map)
  (vector player-x player-y active-colors level-color-map))

(define (level-state-save)
  (make-level-state player-x player-y active-colors level-color-map))

(define (initial-level-state level)
  (make-level-state (level-start-x level) (level-start-y level) '() '()))

(define (level-state-load save)
  (set! player-x (vector-ref save 0))
  (set! player-y (vector-ref save 1))
  (set! active-colors (vector-ref save 2))
  (set! level-color-map (vector-ref save 3))
  (set! win? #f)
  (set! lose? #f)
  (set! ghost-feet-stage
    (if (pair? active-colors)
        (- ghost-feet-sprite-count 1)
        0)))

(define (undo-stack-push)
  (when (= (length undo-stack) undo-stack-max-count)
    (set! undo-stack (list-head undo-stack (- undo-stack-max-count 1))))
  (set! undo-stack (cons (level-state-save) undo-stack)))

(define (undo-stack-empty?)
  (null? undo-stack))

(define (undo-stack-pop)
  (unless (undo-stack-empty?)
    (level-state-load (car undo-stack))
    (set! undo-stack (cdr undo-stack))))

(define (undo-stack-clear)
  (unless (undo-stack-empty?)
    (let loop ((head (car undo-stack))
               (tail (cdr undo-stack)))
      (cond ((null? tail)
             (level-state-load head)
             (set! undo-stack '()))
            (else
             (loop (car tail) (cdr tail)))))))

(define (put-assoc key value alist)
  (let loop ((rlis '())
             (rest alist))
    (cond ((not (pair? rest))
           (cons (cons key value) alist))
          ((and (pair? (car rest)) (equal? (caar rest) key))
           (append (reverse rlis) (cons (cons key value) (cdr rest))))
          (else
           (loop (cons (car rest) rlis) (cdr rest))))))

(define (lerp x y t)
  (+ (* x (- 1 t))
     (* y t)))

(define (in-bounds? x y)
  (and (< -1 x level-width)
       (< -1 y level-height)))

(define (xy->index x y)
  (+ (* y level-width) x))

(define (index->xy index)
  (values (modulo index level-width)
          (quotient index level-width)))

(define level-tile-ref
  (dilambda
    (lambda (x y) (vector-ref (level-tiles the-level) (xy->index x y)))
    (lambda (x y val) (vector-set! (level-tiles the-level) (xy->index x y) val))))

(define (draw-sprite screen-x screen-y width height tint texture index)
  (DrawTexturePro
    texture
    (vector (* index sprite-size) 0 sprite-size sprite-size)
    (letterbox-map/rectangle (vector screen-x screen-y width height))
    #(0 0)
    0
    tint))

(define (button-pressed? screen-x screen-y width height)
  (or (and (IsMouseButtonPressed MOUSE_BUTTON_LEFT)
           (point-in-rect? (GetMouseX) (GetMouseY) screen-x screen-y width height))
      (and (IsGestureDetected GESTURE_TAP)
           (point-in-rect? (GetTouchX) (GetTouchY) screen-x screen-y width height))))

(define (draw-sprite-button screen-x screen-y width height tint texture index)
  (when (point-in-rect? (GetMouseX) (GetMouseY) screen-x screen-y width height)
    (set! tint GOLD))
  (draw-sprite screen-x screen-y width height tint texture-icons (cadr (assoc 'box icon-sprite-indices)))
  (draw-sprite screen-x screen-y width height tint texture index)
  (button-pressed? screen-x screen-y width height))

(define (draw-sprite-number-button screen-x screen-y width height tint num1 num2)
  (draw-sprite screen-x screen-y width height tint texture-icons (cadr (assoc 'box icon-sprite-indices)))
  (draw-sprite screen-x screen-y width height tint texture-icons (cadr (assoc 'dash icon-sprite-indices)))
  (draw-sprite screen-x screen-y width height tint texture-icons
               (+ num1 (cadr (assoc 'digit/0 icon-sprite-indices))))
  (draw-sprite (+ screen-x (/ width 2)) screen-y width height tint texture-icons
               (+ num2 (cadr (assoc 'digit/0 icon-sprite-indices))))
  (button-pressed? screen-x screen-y width height))

(define draw-tile
  (let ()
    ;; Some constants for drawing the color squares.
    ;; They are chosen so that each color gets an equal portion of the tile area.
    (define h31 (sqrt (/ 2 3)))
    (define k31 (- 1 h31))
    (define h41 (sqrt (/ 1 2)))
    (define k41 (- 1 h41))
    (define h51 (sqrt (/ 2 5)))
    (define k51 (- 1 h51))
    (define h52 (sqrt (/ 4 5)))
    (define k52 (- 1 h52))

    (lambda (screen-x screen-y tile)
      (if (symbol? tile)
          (draw-sprite screen-x screen-y tile-screen-size tile-screen-size WHITE
                       texture-tiles (cadr (assoc tile tile-sprite-indices)))
          (let ((colors (map (lambda (x) (list-ref (assoc x color-data) 1)) tile)))
            (define border-pad 1/32)
            (define line-thick 1/32)
            (define (map-coords nx ny)
              (letterbox-map/point
                (vector (+ screen-x (* tile-screen-size (+ 0.5 (* (- nx 0.5) (- 1 (* 2 border-pad))))))
                        (+ screen-y (* tile-screen-size (+ 0.5 (* (- ny 0.5) (- 1 (* 2 border-pad))))))))
              )
            (define (line start end)
              (DrawLineEx start end (* line-thick tile-screen-size) LIGHTGRAY))
            (DrawRectangleRec (letterbox-map/rectangle
                                (vector screen-x screen-y tile-screen-size tile-screen-size))
                              LIGHTGRAY)
            (case (length colors)
              ((1) (DrawTriangle (map-coords 0 0) (map-coords 0 1) (map-coords 1 0) (list-ref colors 0))
                   (DrawTriangle (map-coords 1 1) (map-coords 1 0) (map-coords 0 1) (list-ref colors 0)))
              ((2) (DrawTriangle (map-coords 0 0) (map-coords 0 1) (map-coords 1 0) (list-ref colors 0))
                   (DrawTriangle (map-coords 1 1) (map-coords 1 0) (map-coords 0 1) (list-ref colors 1))
                   (line (map-coords 0 1) (map-coords 1 0)))
              ((3)
               (let* ((h h31) (k k31))
                 (DrawTriangle (map-coords 0 0) (map-coords 0 h) (map-coords h 0) (list-ref colors 0))
                 (DrawTriangle (map-coords 0 1) (map-coords h 0) (map-coords 0 h) (list-ref colors 1))
                 (DrawTriangle (map-coords 0 1) (map-coords 1 0) (map-coords h 0) (list-ref colors 1))
                 (DrawTriangle (map-coords 0 1) (map-coords 1 k) (map-coords 1 0) (list-ref colors 1))
                 (DrawTriangle (map-coords 0 1) (map-coords k 1) (map-coords 1 k) (list-ref colors 1))
                 (DrawTriangle (map-coords 1 1) (map-coords 1 k) (map-coords k 1) (list-ref colors 2))
                 (line (map-coords 0 h) (map-coords h 0))
                 (line (map-coords k 1) (map-coords 1 k))))
              ((4)
               (let* ((h h41) (k k41))
                 (DrawTriangle (map-coords 0 0) (map-coords 0 h) (map-coords h 0) (list-ref colors 0))
                 (DrawTriangle (map-coords 0 1) (map-coords h 0) (map-coords 0 h) (list-ref colors 1))
                 (DrawTriangle (map-coords 0 1) (map-coords 1 0) (map-coords h 0) (list-ref colors 1))
                 (DrawTriangle (map-coords 0 1) (map-coords 1 k) (map-coords 1 0) (list-ref colors 2))
                 (DrawTriangle (map-coords 0 1) (map-coords k 1) (map-coords 1 k) (list-ref colors 2))
                 (DrawTriangle (map-coords 1 1) (map-coords 1 k) (map-coords k 1) (list-ref colors 3))
                 (line (map-coords 0 h) (map-coords h 0))
                 (line (map-coords 0 1) (map-coords 1 0))
                 (line (map-coords k 1) (map-coords 1 k))))
              ((5)
               (let* ((h h51) (k k51) (m h52) (n k52))
                 (DrawTriangle (map-coords 0 0) (map-coords 0 h) (map-coords h 0) (list-ref colors 0))
                 (DrawTriangle (map-coords 0 m) (map-coords h 0) (map-coords 0 h) (list-ref colors 1))
                 (DrawTriangle (map-coords 0 m) (map-coords m 0) (map-coords h 0) (list-ref colors 1))
                 (DrawTriangle (map-coords 0 1) (map-coords m 0) (map-coords 0 m) (list-ref colors 2))
                 (DrawTriangle (map-coords 0 1) (map-coords 1 0) (map-coords m 0) (list-ref colors 2))
                 (DrawTriangle (map-coords 0 1) (map-coords 1 n) (map-coords 1 0) (list-ref colors 2))
                 (DrawTriangle (map-coords 0 1) (map-coords n 1) (map-coords 1 n) (list-ref colors 2))
                 (DrawTriangle (map-coords n 1) (map-coords 1 k) (map-coords 1 n) (list-ref colors 3))
                 (DrawTriangle (map-coords n 1) (map-coords k 1) (map-coords 1 k) (list-ref colors 3))
                 (DrawTriangle (map-coords k 1) (map-coords 1 1) (map-coords 1 k) (list-ref colors 4))
                 (line (map-coords 0 h) (map-coords h 0))
                 (line (map-coords 0 m) (map-coords m 0))
                 (line (map-coords k 1) (map-coords 1 k))
                 (line (map-coords n 1) (map-coords 1 n))))))))))

(define (for-level-xy proc)
  (do ((x 0 (+ x 1)))
      ((= x level-width))
    (do ((y 0 (+ y 1)))
        ((= y level-height))
      (proc x y))))

(define (draw-tiles base-x base-y)
  (for-level-xy
    (lambda (x y)
      (let ((screen-x (+ base-x (* x tile-screen-size)))
            (screen-y (+ base-y (* y tile-screen-size)))
            (tile (level-tile-ref x y)))
        (draw-tile screen-x screen-y tile)))))

(define (draw-player screen-x screen-y)
  (let ((w tile-screen-size)
        (h tile-screen-size))
    (draw-sprite screen-x screen-y tile-screen-size tile-screen-size WHITE texture-ghost-feet ghost-feet-stage)
    (draw-sprite screen-x screen-y tile-screen-size tile-screen-size WHITE texture-ghost-flap ghost-flap-stage)
    (draw-sprite screen-x screen-y tile-screen-size tile-screen-size WHITE
                 texture-ghost-top (cadr (assoc ghost-top-stage ghost-top-sprite-indices)))))

(define (keys-this-frame)
  (let loop ((rkeys '()))
    (let ((key (GetKeyPressed)))
      (if (= key 0)
          (reverse rkeys)
          (loop (cons key rkeys))))))

(define (make-timer interval-secs)
  (let ((elapsed 0.0))
    (lambda args
      (if (null? args)
          elapsed
          (let ((delta-time (car args)))
            (set! elapsed (+ elapsed delta-time))
            (if (< interval-secs elapsed)
                (begin
                  (set! elapsed (- elapsed interval-secs))
                  #t)
                #f))))))

(define (generate-random-board width height)
  (set! level-width width)
  (set! level-height height)
  (set! level-tiles (make-vector (* level-width level-height) 'free))
  (set! level-color-map '())
  (for-level-xy
    (lambda (x y)
      (let* ((choices '(free win lose color wall))
             (color-choices '(red green blue cyan purple))
             (choice (list-ref choices (random (length choices))))
             (tile (if (eq? choice 'color)
                       (let loop ((colors '())
                                  (count (+ 1 (random 5))))
                         (if (zero? count)
                             colors
                             (loop (cons (list-ref color-choices (random (length color-choices)))
                                         colors)
                                   (- count 1))))
                       choice)))
        (set! (level-tile-ref x y) tile))))
  (set! player-x 0)
  (set! player-y 0)
  (set! (level-tile-ref player-x player-y) 'free))

(define (jump)
  (PlaySound sound-jump)
  (let ((anim-time 0.05))
    (animate anim-time
      (lambda ()
        (set! ghost-feet-stage 0))
      (lambda (time)
        (let ((t (/ time anim-time)))
          (set! ghost-feet-stage (inexact->exact (floor (* t ghost-feet-sprite-count))))
          ))
      (lambda ()
        (set! ghost-feet-stage (- ghost-feet-sprite-count 1))))))

(define (land)
  (PlaySound sound-land)
  (let ((anim-time 0.05))
    (animate anim-time
      (lambda ()
        (set! ghost-feet-stage (- ghost-feet-sprite-count 1)))
      (lambda (time)
        (let ((t (/ time anim-time)))
          (set! ghost-feet-stage (floor (* (- 1 t) ghost-feet-sprite-count)))))
      (lambda ()
        (set! ghost-feet-stage 0)))))

(define (can-slide? dir)
  (let* ((deltas (cdr (assoc dir input-delta-xy)))
         (new-x (+ player-x (list-ref deltas 0)))
         (new-y (+ player-y (list-ref deltas 1))))
    (and (in-bounds? new-x new-y)
         (not (eq? (level-tile-ref new-x new-y) 'wall)))))

(define (slide dir)
  (let* ((deltas (cdr (assoc dir input-delta-xy)))
         (old-x player-x)
         (old-y player-y)
         (new-x (+ player-x (list-ref deltas 0)))
         (new-y (+ player-y (list-ref deltas 1)))
         (sliding? (can-slide? dir)))
    (let ((anim-time 0.05)
          (cooldown 0.05))
      (animate (+ anim-time cooldown)
        (lambda ()
          (set! player-animating? #t)
          (PlaySound (cond ((pair? active-colors) sound-color-slide)
                           (sliding? sound-slide)
                           (else sound-bump))))
        (lambda (time)
          (let* ((t (min 1 (/ time anim-time)))
                 (t (if sliding?
                        t
                        (- 0.5 (abs (- 0.5 t))))))
            (set! player-screen-x (* (lerp old-x new-x t) tile-screen-size))
            (set! player-screen-y (* (lerp old-y new-y t) tile-screen-size))))
        (lambda ()
          (set! player-animating? #f))))
    (when sliding?
      (set! player-x new-x)
      (set! player-y new-y))))

(define (point-in-rect? x y rect-x rect-y rect-w rect-h)
  (let* ((rect (vector rect-x rect-y rect-w rect-h))
         (real-rect (letterbox-map/rectangle rect))
         (real-x (vector-ref real-rect 0))
         (real-y (vector-ref real-rect 1))
         (real-w (vector-ref real-rect 2))
         (real-h (vector-ref real-rect 3)))
    (and (<= 0 (- x real-x) real-w)
         (<= 0 (- y real-y) real-h))))

(define (get-input)
  (call-with-exit
    (lambda (return)
      (for-each
        (lambda (keys)
          (for-each
            (lambda (key)
              (when (IsKeyPressed key)
                (return (car keys))))
            (cdr keys)))
        input-keys)
      (for-each
        (lambda (gestures)
          (for-each
            (lambda (gesture)
              (when (and (IsGestureDetected gesture))
                (return (car gestures))))
            (cdr gestures)))
        input-gestures)
      (return #f))))

(define (reduce)
  (call-with-exit
    (lambda (return)
      (assert 'reduce (not win?))
      (assert 'reduce (not lose?))
      (define (input-or-fail)
        (let ((input (get-input)))
          (unless (member input '(up down left right))
            (return #f))
          input))
      (define (slide-with-color color)
        (let ((mapping (assoc color level-color-map)))
          (if mapping
              (slide (cadr mapping))
              (let ((dir (input-or-fail)))
                (undo-stack-push)
                (slide dir)
                (set! level-color-map (put-assoc color (list dir) level-color-map))
                ))))
      (define (step-active-color)
        (slide-with-color (car active-colors))
        (set! active-colors (cdr active-colors))
        (when (null? active-colors)
          (land))
        (return #t))
      (when (pair? active-colors)
        (step-active-color))
      (let ((tile (level-tile-ref player-x player-y)))
        (when (pair? tile)
          (set! active-colors tile)
          (jump)
          (return #t))
        (case tile
          ((free)
           (let ((input (input-or-fail)))
             (when (can-slide? input)
               (undo-stack-push))
             (slide input)))
          ((win)
           (set! win? #t)
           (PlaySound sound-win))
          ((lose)
           (set! lose? #t)
           (PlaySound sound-lose))
          ((event)
           (set! event? #t))
          (else (error 'reduce "Undefined reducton")))
        (return #t)))))

(define (animate time-secs on-enter on-frame on-leave)
  (define new-animator
    (let ((t 0.0))
      (lambda (delta-time)
        (set! t (+ t delta-time))
        (cond
          ((zero? t) (values))
          ((< t time-secs) (on-frame t))
          (else
           (on-leave)
           (set! animator #f))))))
  (define (update-animator)
    (on-enter)
    (set! animator new-animator))
  (let ((old-animator animator))
    (if old-animator
        (set! animator
          (lambda (delta-time)
            (old-animator delta-time)
            (unless animator
              (update-animator))))
        (update-animator))))

(define (load-levels-from-data)
  (set! levels
    (list->vector
      (sort!
        (map (lambda (args)
               (apply parse-level args))
             level-data)
        (lambda (x y)
          (let ((c1 (- (level-major x) (level-major y))))
            (if (= c1 0)
                (< (level-minor x) (level-minor y))
                (< c1 0))))))))

(define (next-level?)
  (< the-level-index (- (vector-length levels) 1)))

(define (load-next-level)
  (level-load (+ the-level-index 1)))

(define (init)
  (SetTraceLogLevel LOG_WARNING)
  (SetGesturesEnabled (apply logior (apply append (map cdr input-gestures))))
  (InitAudioDevice)
  (InitWindow window-width window-height window-title)
  (SetWindowState FLAG_WINDOW_RESIZABLE)
  (SetTargetFPS 60)

  (set! font-default (GetFontDefault))
  (set! music (LoadMusicStream "asset/Lines of Code.mp3"))
  (set! sound-bump (LoadSound "asset/bump.wav"))
  (set! sound-color-slide (LoadSound "asset/color-slide.wav"))
  (set! sound-jump (LoadSound "asset/jump.wav"))
  (set! sound-land (LoadSound "asset/land.wav"))
  (set! sound-lose (LoadSound "asset/lose.wav"))
  (set! sound-slide (LoadSound "asset/slide.wav"))
  (set! sound-win (LoadSound "asset/win.wav"))
  (set! texture-cover (LoadTexture "asset/cover.png"))
  (set! texture-ghost-top (LoadTexture "asset/ghost-top.png"))
  (set! texture-ghost-flap (LoadTexture "asset/ghost-flap.png"))
  (set! texture-ghost-feet (LoadTexture "asset/ghost-feet.png"))
  (set! texture-tiles (LoadTexture "asset/tiles.png"))
  (set! texture-icons (LoadTexture "asset/icons.png"))
  (set! image-game-icon (LoadImage "asset/icon.png"))

  (load-levels-from-data)

  (PlayMusicStream music)
  (SetMusicVolume music 0.75)
  (SetWindowIcon image-game-icon)

  (values))

(define (fini)
  (values))

(define (take)
  ;(generate-random-board 20 20)
  (load "scm/levels.scm")
  (load-levels-from-data)
  (when the-level-index
    (level-load the-level-index))

  (values))

(define (pass)
  (values))

(define (done?)
  (or (and (not wasm?) (WindowShouldClose))
      should-exit?))

(define (step)
  (dynamic-wind
    (lambda () #f)
    update
    (lambda ()
      (dynamic-wind BeginDrawing draw EndDrawing))))

(define (update)
  (define delta-time (GetFrameTime))

  (letterbox-update)

  (UpdateMusicStream music)
  (let ((music-loop-begin 48.73)
        (music-loop-end 150.0))
    (when (>= (GetMusicTimePlayed music) music-loop-end)
      (SeekMusicStream music music-loop-begin)))

  (case game-state
    ((main-menu) (update-main-menu delta-time))
    ((selection) (update-selection delta-time))
    ((level) (update-level delta-time))))

(define (update-main-menu delta-time)
  (case (get-input)
    ((confirm) (set! game-state 'selection))
    ((cancel) (set! should-exit? #t))
    (else (values)))
  (values))

(define (update-selection delta-time)
  (case (get-input)
    ((cancel)
     (set! game-state 'main-menu)
     (set! selection-major 1)
     (set! selection-minor 1))
    ((confirm)
     (when (and selection-major selection-minor)
       (let ((major selection-major)
             (minor selection-minor))
           (set! selection-major 1)
           (set! selection-minor 1)
           (set! game-state 'level)
           (level-load
             (level-major-minor->index major minor)))))
    (else (values)))
  (let ((input (get-input)))
    (when (member input '(up down left right))
      (let* ((deltas (cdr (assoc input input-delta-xy)))
             (new-major (if selection-major
                            (+ selection-major (list-ref deltas 1))
                            1))
             (new-minor (if selection-minor
                            (+ selection-minor (list-ref deltas 0))
                            1)))
        (when (level-major-minor->index new-major new-minor)
          (set! selection-major new-major)
          (set! selection-minor new-minor)))))
  (values))

(define (update-level delta-time)
  (when animator
    (animator delta-time))
  (begin
    (set! ghost-flap-time (+ ghost-flap-time delta-time))
    (let loop ()
      (when (> ghost-flap-time ghost-flap-frame-time)
        (set! ghost-flap-time (- ghost-flap-time ghost-flap-frame-time))
        (set! ghost-flap-stage (modulo (+ ghost-flap-stage ghost-flap-direction) ghost-flap-sprite-count))
        (loop))))
  (begin
    (set! ghost-flap-direction-time (+ ghost-flap-direction-time delta-time))
    (let loop ()
      (when (> ghost-flap-direction-time ghost-flap-direction-switch-time)
        (set! ghost-flap-direction-time (- ghost-flap-direction-time ghost-flap-direction-switch-time))
        (set! ghost-flap-direction (if (< (random 1.0) 0.5) +1 -1)))))
  (begin
    (set! ghost-blink-time (+ ghost-blink-time delta-time))
    (let loop ()
      (when (> ghost-blink-time ghost-blink-frame-time)
        (set! ghost-blink-time (- ghost-blink-time ghost-blink-frame-time))
        (case ghost-blink-stage
          ((idle)
           (if (zero? ghost-blink-interval)
               (set! ghost-blink-stage 'squint-in)
               (set! ghost-blink-interval (- ghost-blink-interval 1))))
          ((squint-in)
           (set! ghost-blink-stage 'blink))
          ((blink)
           (set! ghost-blink-stage 'squint-out))
          ((squint-out)
           (set! ghost-blink-stage 'idle)
           (set! ghost-blink-interval
             (inexact->exact
              (floor (+ ghost-blink-interval-min
                        (random (- ghost-blink-interval-max
                                   ghost-blink-interval-min)))))))
          (else (error 'update "Invalid ghost blink stage ~a\n" ghost-blink-stage)))
        (set! ghost-top-stage (cadr (assoc ghost-blink-stage ghost-top-stages)))
        (loop))))
  (unless (or animator win? lose?)
    (reduce)
    (when win?
      (level-win! the-level)))
  (unless player-animating?
    (set! player-screen-x (* player-x tile-screen-size))
    (set! player-screen-y (* player-y tile-screen-size)))

  (case (get-input)
    ((confirm)
     (when (and win? (next-level?))
       (load-next-level)))
    ((cancel)
     (set! game-state 'selection))
    ((undo)
     (undo-stack-pop))
    ((reset)
     (undo-stack-clear))
    (else (values)))

  (values))

(define (draw)
  (ClearBackground BLACK)
  (case game-state
    ((main-menu) (draw-main-menu))
    ((selection) (draw-selection))
    ((level) (draw-level)))
  ;(DrawFPS 15 45)
  )

(define (draw-text text x y font-size tint)
  (define real-position (letterbox-map/point (vector x y)))
  (define real-font-size (letterbox-map/font-size font-size))
  (define real-spacing (/ real-font-size 10))
  (DrawTextEx font-default text real-position real-font-size real-spacing tint))

(define (draw-main-menu)
  (DrawTexturePro
    texture-cover
    (vector 0 0 (* 7 sprite-size) (* 7 sprite-size))
    (letterbox-map/rectangle
      (vector 0 (* 1 tile-screen-size) (* 7 tile-screen-size) (* 7 tile-screen-size)))
    #(0 0) 0 WHITE)
  (draw-text "GHOSTHOP" 7 15 32 WHITE)
  (draw-text "A game by Mason Cutting" 7 (+ 10 (* 8 tile-screen-size)) 24 WHITE)
  (draw-text "Music by Trevor Lentz" 7 (+ 35 (* 8 tile-screen-size)) 24 WHITE)

  (let ((s tile-screen-size))
    (when (draw-sprite-button (* 4 s) (* 0 s) s s WHITE texture-icons (cadr (assoc 'cancel icon-sprite-indices)))
      (set! should-exit? #t))

    (when (draw-sprite-button (* 3 s) (* 0 s) s s WHITE texture-icons (cadr (assoc 'play icon-sprite-indices)))
      (set! game-state 'selection))
    )
  (values))

(define (draw-selection)
  (draw-text "LEVELS" 7 15 32 WHITE)

  (let ((s tile-screen-size))
    (when (draw-sprite-button (* 4 s) 0 s s WHITE texture-icons (cadr (assoc 'cancel icon-sprite-indices)))
      (set! game-state 'main-menu))

    (for-each
      (lambda (index level)
        (let* ((major (level-major level))
               (minor (level-minor level))
               (tint (cond ((and (eqv? selection-major major)
                                 (eqv? selection-minor minor))
                            GOLD)
                           ((level-won? level) GREEN)
                           (else WHITE)))
               (x (* s (- minor 1)))
               (y (* s (+ top-height (- major 1)))))
          (when (point-in-rect? (GetMouseX) (GetMouseY) x y s s)
            (set! selection-major major)
            (set! selection-minor minor))
          (when (draw-sprite-number-button x y s s tint major minor)
            (set! game-state 'level)
            (level-load index))))
      (iota (vector-length levels))
      levels))
  (values))

(define (draw-level)
  (draw-tiles 0 (* top-height tile-screen-size))
  (draw-player player-screen-x (+ player-screen-y (* top-height tile-screen-size)))

  (draw-text (format #f "~a-~a: ~a" (level-major the-level) (level-minor the-level) (level-name the-level))
             10 19 24 WHITE)

  (let ((s tile-screen-size))
    (when (draw-sprite-button (* 4 s) 0 s s WHITE texture-icons (cadr (assoc 'home icon-sprite-indices)))
      (set! game-state 'selection))

    (when (draw-sprite-button (* 5 s) 0 s s WHITE texture-icons (cadr (assoc 'reset icon-sprite-indices)))
      (undo-stack-pop))

    (when (draw-sprite-button (* 6 s) 0 s s WHITE texture-icons (cadr (assoc 'rewind icon-sprite-indices)))
      (undo-stack-clear))

    (when (and win?
               (next-level?)
               (draw-sprite-button (* 3 s) 0 s s WHITE texture-icons
                                   (cadr (assoc 'play icon-sprite-indices))))
      (load-next-level))

    ;; Color map
    (let ((start-x (* 1/2 (- (* level-width tile-screen-size) (* s (length level-colors))))))
      (let loop ((i 0)
                 (colors level-colors))
        (unless (null? colors)
          (let* ((color (car colors))
                 (color-icon
                   (cond ((assoc color level-color-map) => cadr)
                         ((and (pair? active-colors) (eq? color (car active-colors))) '?)
                         (else #f)))
                 (x (+ start-x (* i s)))
                 (y (* (+ top-height level-height) s))
                 (w s)
                 (h s))
            (DrawTexturePro
              texture-icons
              (vector (* (cadr (assoc 'box icon-sprite-indices)) sprite-size) 0 sprite-size sprite-size)
              (letterbox-map/rectangle (vector x y w h))
              #(0 0)
              0
              (cadr (assoc color color-data)))
            (when color-icon
              (draw-sprite x y w h WHITE texture-icons (cadr (assoc color-icon icon-sprite-indices))))
            (loop (+ i 1) (cdr colors)))))))
  (values))

