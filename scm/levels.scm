(define level-data
  '(
    (1 1 "Result" 0 3
     ()
     ".######
      .#@___#
      .####_#
      ______.
      .......
      .......
      .......")
    (1 2 "Recall" 0 0
     ()
     "_r_....
      .._rrr_
      ......_
      _______
      _......
      rrrrrr@
      .......")
    (1 3 "Revise" 3 6
     ()
     ".......
      ..#r@..
      ..._...
      ..._...
      ..___..
      .._r_..
      ..._...")
    (1 4 "Reselect" 3 0
     ()
     "._r_g_.
      ._..._.
      ._____.
      .r.....
      .ggr...
      ...gr..
      ....@..")
    (1 5 "Repeat" 0 0
     ((#\1 red red)
      (#\2 green green)
      (#\3 blue blue blue))
     "_r_....
      .__1._.
      ...#._.
      ...3.2.
      .....#.
      .......
      ...@...")
    (1 6 "Reorient" 2 4
     ((#\1 red green)
      (#\2 red red blue)
      (#\3 blue cyan))
     "._3....
      .#.3...
      2#.#_..
      _..#@..
      #1_#...
      ...#...
      ...#...")
    (1 7 "Recover" 0 5
     ((#\1 red green green blue))
     "..__1..
      .._...@
      .#____#
      .##.._#
      ....._#
      _1#_1#.
      .......")
    (2 1 "Converge" 3 0
     ()
     "..._...
      ._grc_.
      ._.b__.
      .___...
      ...c...
      .brg...
      .@.....")
    (2 2 "Confer" 1 1
     ()
     "#######
      #_g_c_#
      #r#b#p#
      #p_p#g#
      ##_##_#
      #_p##p#
      #__@__#")
    (2 3 "Contain" 3 0
     ()
     "..._...
      .gbrcg.
      .rp#pr.
      .pr#gp.
      .cp#br.
      .bcbgp.
      ...@...")
    (2 4 "Construct" 0 0
     ()
     "_r.....
      bgr....
      .br__b#
      .___.g#
      .###.r#
      .....@.
      .......")
    (2 5 "Conduce" 3 3
     ()
     "grrrrr@
      g.rgg.g
      gr#r#rg
      ggr_rgg
      g.#r#rg
      g..gr.g
      @rrrrrr")
    (2 6 "Consult" 0 3
     ()
     "...c_pg
      .b_g.rg
      ._.b_r_
      _r....@
      ._.p_g_
      .c_g..g
      ...b_cr")
    (2 7 "Contrive" 0 0
     ()
     "_.cg#pb
      rpc.rp_
      c_grbc.
      brpc#bc
      cbbr.bp
      .gpgbr@
      ##..#g.")
    (3 1 "Dissect" 0 0
     ((#\1 red green blue)
      (#\2 purple cyan)
      (#\3 green blue red)
      (#\4 red red blue blue)
      (#\5 blue green green cyan))
     "_1..2..
      #1.3.#.
      #4...5.
      .......
      ..#@#..
      ..###..
      .......")
    (3 2 "Diverge" 3 0
     ((#\1 blue cyan blue green blue)
      (#\2 green green red green blue)
      (#\3 cyan cyan purple cyan green)
      (#\4 purple cyan red cyan cyan)
      (#\5 purple red green cyan))
     "..._...
      ._rgb_.
      ._._._.
      #1#2#3#
      .......
      ...#...
      .4.@.5.")
    (3 3 "Direct" 0 0
     ((#\1 red green blue cyan)
      (#\2 cyan cyan red green blue))
     "_1#....
      ._.....
      .1..#..
      ...2#..
      ..#.#..
      ..#...@
      .......")
    (3 4 "Divulge" 0 3
     ((#\1 red green blue cyan purple)
      (#\2 red blue green green))
     "....2..
      ....1#.
      .###.#@
      _11..##
      .#.#...
      ..#....
      .......")
    ;; > v < v >
    (3 5 "Divest" 2 0
     ((#\1 red green blue cyan purple)
      (#\2 green blue cyan purple red)
      (#\3 blue cyan purple red green)
      (#\4 cyan purple red green blue)
      (#\5 purple red green blue cyan)
      ())
     ".._....
      ..1....
      .#2#...
      ...34.#
      .##.#5.
      .....#.
      ......@")
    ;; ^ < < v >
    (3 6 "Dilute" 3 3
     ((#\1 red green blue cyan purple)
      (#\2 green cyan purple cyan blue)
      (#\3 purple purple cyan purple red)
      (#\4 purple red purple purple red)
      (#\5 cyan green purple cyan cyan))
     "#####..
      #...#..
      #.21#..
      ..#_#.5
      3..#...
      #..4...
      ##....@")
    ;; v > < ^ >
    (3 7 "Diatribe" 0 0
     ((#\1 red green blue cyan purple)
      (#\2 cyan purple red green blue)
      (#\3 red blue red green green)
      (#\4 purple red blue blue cyan)
      (#\5 red green cyan green red)
      (#\6 cyan green green cyan blue)
      (#\7 red red red red green)
      (#\8 blue blue cyan red blue)
      (#\9 blue blue red blue cyan)
      )
     "_..####
      123...#
      .....7.
      #.54...
      #...6.#
      #####.#
      @..9..8")
    ))

