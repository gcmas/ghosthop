(define (id x) x)
(define (tostring x)
  (cond ((string? x) x)
        ((symbol? x) (symbol->string x))
        (else (error 'tostring "Cannot convert ~a to string" x))))
(define (tosymbol x)
  (cond ((symbol? x) x)
        ((string? x) (string->symbol x))
        (else (error 'tosymbol "Cannot convert ~a to symbol" x))))
(define (iota n)
  (let loop ((n (- n 1))
             (items '()))
    (if (< n 0)
        items
        (loop (- n 1) (cons n items)))))

; cname -> cdecl
(define cdecls (make-hash-table))
(define cincludes '())
(define cprotos '())
(define cdefns '())
(define cinits '())

(define (add-cinclude x) (set! cincludes (cons x cincludes)))
(define (add-cproto x) (set! cprotos (cons x cprotos)))
(define (add-cdefn x) (set! cdefns (cons x cdefns)))
(define (add-cinit x) (set! cinits (cons x cinits)))

(define (cdecl? x)
  (and (vector? x) (memq (x 0) '(<cconst> <ctype> <cproc>))))
(define (cdecl-name x) (x 1))

(define (cconst name type)
  (vector '<cconst> (tosymbol name) (toctype type)))
(define (cconst? cs)
  (and (vector? cs) (eq? (cs 0) '<cconst>)))
(define (cconst-name cs) (cs 1))
(define (cconst-type cs) (cs 2))

(define (ctype name make pred cname cident cpred s7->c c->s7 cerr cproto cdefn cinit csig)
  (vector '<ctype> (tosymbol name) make pred cname cident cpred s7->c c->s7 cerr cproto cdefn cinit csig))
(define (ctype? ty)
  (and (vector? ty) (eq? (ty 0) '<ctype>)))
(define (ctype-name ty) (ty 1))
(define (ctype-make ty) (ty 2))
(define (ctype-pred ty) (ty 3))
(define (ctype-cname ty) (ty 4))
(define (ctype-cident ty) (ty 5))
(define (ctype-cpred ty) (ty 6))
(define (ctype-s7->c ty) (ty 7))
(define (ctype-c->s7 ty) (ty 8))
(define (ctype-cerr ty) (ty 9))
(define (ctype-cproto ty) (ty 10))
(define (ctype-cdefn ty) (ty 11))
(define (ctype-cinit ty) (ty 12))
(define (ctype-csig ty) (ty 13))

(define (toctype x)
  (if (or (ctype? x) (eq? x 'void))
      x
      (cdecls x)))

(define (cproc name params return)
  (vector '<cproc> (tosymbol name) (map toctype params) (toctype return)))
(define (cproc? pr)
  (and (vector? pr) (eq? (pr 0) '<cproc>)))
(define (cproc-name pr) (pr 1))
(define (cproc-params pr) (pr 2))
(define (cproc-return pr) (pr 3))

(define (register-cdecl decl)
  (set! (cdecls (cdecl-name decl)) decl))

(define (register-ctype . args)
  (register-cdecl (apply ctype args)))

(define (register-cproc . args)
  (register-cdecl (apply cproc args)))

(add-cproto "static s7_pointer integer_string;\n")
(add-cinit "  integer_string = s7_make_semipermanent_string(sc, \"an integer\");\n")
(add-cproto "static int is_integer(s7_scheme *sc, s7_pointer p);\n")
(add-cdefn "static int is_integer(s7_scheme *sc, s7_pointer p) { return (int) s7_is_integer(p); }\n")
(add-cproto "static s7_pointer sig_integer(s7_scheme *sc);\n")
(add-cdefn "static s7_pointer sig_integer(s7_scheme *sc) { return s7_make_symbol(sc, \"integer?\"); }\n")

(define (ctype-integer name cname cident)
  (let ((name-str (symbol->string name))
        (cname-str (symbol->string cname))
        (cident-str (symbol->string cident)))
    (ctype
      name
      id
      integer?
      cname
      cident
      "is_integer"
      (format #f "from_~a" cident-str)
      (format #f "into_~a" cident-str)
      "integer_string"
      (format #f
        (append
          "static ~a from_~a(s7_scheme *sc, s7_pointer p);\n"
          "static s7_pointer into_~a(s7_scheme *sc, ~a x);\n")
        cname-str cident-str cident-str cname-str)
      (format #f
        (append
          "static ~a from_~a(s7_scheme *sc, s7_pointer p) { return (~a) s7_integer(p); }\n"
          "static s7_pointer into_~a(s7_scheme *sc, ~a x) { return s7_make_integer(sc, (s7_int) x); }\n")
        cname-str cident-str cname-str cident-str cname-str)
      ""
      "sig_integer"
      )))

(define ctype-string
  (ctype
    'string
    id
    string?
    (symbol "const char*")
    'string
    "is_string"
    "from_string"
    "into_string"
    "string_string"
    (append
      "static s7_pointer string_string;\n"
      "static int is_string(s7_scheme *sc, s7_pointer p);\n"
      "static char const *from_string(s7_scheme *sc, s7_pointer p);\n"
      "static s7_pointer into_string(s7_scheme *sc, char const *s);\n"
      "static s7_pointer sig_string(s7_scheme *sc);\n")
    (append
      "static int is_string(s7_scheme *sc, s7_pointer p) { return (int) s7_is_string(p); }\n"
      "static char const *from_string(s7_scheme *sc, s7_pointer p) { return s7_string(p); }\n"
      "static s7_pointer into_string(s7_scheme *sc, char const *s) { return s7_make_string(sc, s); }\n"
      "static s7_pointer sig_string(s7_scheme *sc) { return s7_make_symbol(sc, \"string?\"); }\n")
    "  string_string = s7_make_semipermanent_string(sc, \"a string\");\n"
    "sig_string"
    ))

(define ctype-boolean
  (ctype
    'boolean
    id
    boolean?
    'bool
    'boolean
    "is_boolean"
    "from_boolean"
    "into_boolean"
    "boolean_string"
    (append
      "static s7_pointer boolean_string;\n"
      "static int is_boolean(s7_scheme *sc, s7_pointer p);\n"
      "static bool from_boolean(s7_scheme *sc, s7_pointer p);\n"
      "static s7_pointer into_boolean(s7_scheme *sc, bool x);\n"
      "static s7_pointer sig_boolean(s7_scheme *sc);\n")
    (append
      "static int is_boolean(s7_scheme *sc, s7_pointer p) { return (int) s7_is_boolean(p); }\n"
      "static bool from_boolean(s7_scheme *sc, s7_pointer p) { return s7_boolean(sc, p); }\n"
      "static s7_pointer into_boolean(s7_scheme *sc, bool x) { return s7_make_boolean(sc, x); }\n"
      "static s7_pointer sig_boolean(s7_scheme *sc) { return s7_make_symbol(sc, \"boolean?\"); }\n")
    "  boolean_string = s7_make_semipermanent_string(sc, \"a boolean\");\n"
    "sig_boolean"
    ))

(add-cproto "static s7_pointer real_string;\n")
(add-cinit "  real_string = s7_make_semipermanent_string(sc, \"a real\");\n")
(add-cproto "static int is_real(s7_scheme *sc, s7_pointer p);\n")
(add-cdefn "static int is_real(s7_scheme *sc, s7_pointer p) { return (int) s7_is_real(p); }\n")
(add-cproto "static s7_pointer sig_real(s7_scheme *sc);\n")
(add-cdefn "static s7_pointer sig_real(s7_scheme *sc) { return s7_make_symbol(sc, \"real?\"); }\n")

(define (ctype-real name cname cident)
  (let ((name-str (symbol->string name))
        (cname-str (symbol->string cname))
        (cident-str (symbol->string cident)))
    (ctype
      name
      id
      real?
      cname
      cident
      "is_real"
      (format #f "from_~a" cident-str)
      (format #f "into_~a" cident-str)
      "real_string"
      (format #f
        (append
          "static ~a from_~a(s7_scheme *sc, s7_pointer p);\n"
          "static s7_pointer into_~a(s7_scheme *sc, ~a x);\n")
        cname-str cident-str cident-str cname-str)
      (format #f
        (append
          "static ~a from_~a(s7_scheme *sc, s7_pointer p) { return (~a) s7_real(p); }\n"
          "static s7_pointer into_~a(s7_scheme *sc, ~a x) { return s7_make_real(sc, (s7_double) x); }\n")
        cname-str cident-str cname-str cident-str cname-str)
      ""
      "sig_real"
      )))

(define ctype-void*
  (ctype
    'void*
    c-pointer
    c-pointer?
    'void*
    'c_pointer
    "is_c_pointer"
    "from_c_pointer"
    "into_c_pointer"
    "c_pointer_string"
    (append
      "static s7_pointer c_pointer_string;\n"
      "static int is_c_pointer(s7_scheme *sc, s7_pointer p);\n"
      "static void *from_c_pointer(s7_scheme *sc, s7_pointer p);\n"
      "static s7_pointer into_c_pointer(s7_scheme *sc, void *x);\n"
      "static s7_pointer sig_c_pointer(s7_scheme *sc);\n")
    (append
      "static int is_c_pointer(s7_scheme *sc, s7_pointer p) { return s7_is_c_pointer(p); }\n"
      "static void *from_c_pointer(s7_scheme *sc, s7_pointer p) { return s7_c_pointer(p); }\n"
      "static s7_pointer into_c_pointer(s7_scheme *sc, void *ptr) { return s7_make_c_pointer(sc, ptr); }\n"
      "static s7_pointer sig_c_pointer(s7_scheme *sc) { return s7_make_symbol(sc, \"c-pointer?\"); }\n")
    "  c_pointer_string = s7_make_semipermanent_string(sc, \"a c-pointer\");\n"
    "sig_c_pointer"
    ))

(define (ctype-struct name make pred cname cident fields)
  (define (if-scalar type scalar-proc array-proc)
    (if (and (pair? type) (eq? (car type) 'array))
        (array-proc (cadr type) (caddr type))
        (scalar-proc type)))
  (define (field-type-scalar field-type)
    (if-scalar field-type
      (lambda (type) type)
      (lambda (count type) (field-type-scalar type))))
  (let ((name-str (symbol->string name))
        (pred-str (symbol->string pred))
        (cname-str (symbol->string cname))
        (cident-str (symbol->string cident))
        (field-names (map car fields))
        (field-types (map (lambda (field)
                            (let f ((type (cadr field)))
                              (if-scalar type
                                toctype
                                (lambda (count type)
                                  `(array ,count ,(f type))))))
                          fields))
        (nfields (length fields)))
    (ctype
      name
      vector
      vector?
      cname
      cident
      (format #f "is_~a" cident-str)
      (format #f "from_~a" cident-str)
      (format #f "into_~a" cident-str)
      (format #f "~a_string" cident-str)
      (format #f
        (append
          "static s7_pointer ~a_string;\n"
          "static int is_~a(s7_scheme *sc, s7_pointer p);\n"
          "static ~a from_~a(s7_scheme *sc, s7_pointer p);\n"
          "static s7_pointer into_~a(s7_scheme *sc, ~a x);\n"
          "static s7_pointer sig_~a(s7_scheme *sc);\n")
        cident-str cident-str cname-str cident-str cident-str cname-str cident-str)
      (append
        (format #f "static int is_~a(s7_scheme *sc, s7_pointer p0) {\n" cident-str)
        (format #f "  if (!s7_is_vector(p0) || s7_vector_length(p0) != ~a) return 0;\n" nfields)
        (apply append
          (map
            (lambda (i field-type)
              (let f ((type field-type)
                      (i i)
                      (depth 1)
                      (indent "  "))
                (if-scalar type
                  (lambda (type)
                    (format #f "~aif (!~a(sc, s7_vector_ref(sc, p~a, ~a))) return 0;\n"
                            indent (ctype-cpred type) (- depth 1) i))
                  (lambda (count type)
                    (append
                      (format #f "~a{\n" indent)
                      (format #f "~a  s7_pointer p~a = s7_vector_ref(sc, p~a, ~a);\n"
                              indent depth (- depth 1) i)
                      (format #f "~a  if (!s7_is_vector(p~a) || s7_vector_length(p~a) != ~a) return 0;\n"
                              indent depth depth count)
                      (apply append
                        (map
                          (lambda (j)
                            (f type j (+ depth 1) (append indent "  ")))
                          (iota count)))
                      (format #f "~a}\n" indent))))))
            (iota nfields)
            field-types))
        "  return 1;\n"
        "}\n"

        (format #f "static ~a from_~a(s7_scheme *sc, s7_pointer p0) {\n" cname-str cident-str)
        (format #f "  ~a retval;\n" cname-str)
        (apply append
          (map
            (lambda (i field-name field-type)
              (let f ((type field-type)
                      (i i)
                      (lhs (format #f "retval.~a" (symbol->string field-name)))
                      (depth 1)
                      (indent "  "))
                (if-scalar type
                  (lambda (type)
                    (format #f "~a~a = ~a(sc, s7_vector_ref(sc, p~a, ~a));\n"
                            indent lhs (ctype-s7->c type) (- depth 1) i))
                  (lambda (count type)
                    (append
                      (format #f "~a{\n" indent)
                      (format #f "~a  s7_pointer p~a = s7_vector_ref(sc, p~a, ~a);\n"
                              indent depth (- depth 1) i)
                      (apply append
                        (map
                          (lambda (j)
                            (f type j (format #f "~a[~a]" lhs j) (+ depth 1) (append indent "  ")))
                          (iota count)))
                      (format #f "~a}\n" indent))))))
            (iota nfields)
            field-names
            field-types))
        "  return retval;\n"
        "}\n"

        (format #f "static s7_pointer into_~a(s7_scheme *sc, ~a x) {\n" cident-str cname-str)
        (format #f "  s7_pointer p0 = s7_make_vector(sc, ~a);\n" nfields)
        (apply append
          (map
            (lambda (i field-name field-type)
              (let f ((type field-type)
                      (i i)
                      (rhs (format #f "x.~a" (symbol->string field-name)))
                      (depth 1)
                      (indent "  "))
                (if-scalar type
                  (lambda (type)
                    (format #f "~as7_vector_set(sc, p~a, ~a, ~a(sc, ~a));\n"
                            indent (- depth 1) i (ctype-c->s7 type) rhs))
                  (lambda (count type)
                    (append
                      (format #f "~a{\n" indent)
                      (format #f "~a  s7_pointer p~a = s7_make_vector(sc, ~a);\n"
                              indent depth count)
                      (apply append
                        (map
                          (lambda (j)
                            (f type j (format #f "~a[~a]" rhs j) (+ depth 1) (append indent "  ")))
                          (iota count)))
                      (format #f "~a  s7_vector_set(sc, p~a, ~a, p~a);\n" indent (- depth 1) i depth)
                      (format #f "~a}\n" indent))))))
            (iota nfields)
            field-names
            field-types))
        "  return p0;\n"
        "}\n"

        (format #f "static s7_pointer sig_~a(s7_scheme *sc) { return s7_make_symbol(sc, \"~a\"); }\n"
                cident-str pred-str)
        (apply append
          (map
            (lambda (field-name field-type)
              (let ((scalar-type (field-type-scalar field-type))
                    (field-name-str (symbol->string field-name)))
                (append
                  (format #f "static s7_pointer field_~a_slash_~a(s7_scheme *sc, s7_pointer args) {\n"
                          cident-str field-name-str)
                  "  s7_pointer p = s7_car(args);"
                  "  void *field_ptr;\n"
                  "  if (!s7_is_c_pointer(p))\n"
                  "    return s7_wrong_type_error(sc,\n"
                  (let ((accessor-name (format #f "~a/~a" name-str field-name-str)))
                    (format #f "      s7_make_string_wrapper_with_length(sc, \"~a\", ~a),\n"
                            accessor-name (string-length accessor-name)))
                  "      0, p, c_pointer_string);\n"
                  (format #f "  field_ptr = &((~a *) s7_c_pointer(p))->~a;\n"
                          cname-str field-name-str)
                  "  return s7_make_c_pointer_with_type(sc, field_ptr,\n"
                  (format #f "    s7_make_symbol(sc, \"~a\"), s7_nil(sc));\n"
                          (symbol->string (ctype-name scalar-type)))
                  "}\n"
                  )))
            field-names
            field-types)))
      (append
        (format #f "  ~a_string = s7_make_semipermanent_string(sc, \"a ~a\");\n" cident-str name-str)
        (apply append
          (map
            (lambda (field-name field-type)
              (let ((field-name-str (symbol->string field-name)))
                (append
                  "  s7_define(sc, curlet,\n"
                  (format #f "    s7_make_symbol(sc, \"~a/~a\"),\n" name-str field-name-str)
                  (format #f "    s7_make_typed_function(sc, \"~a/~a\",\n" name-str field-name-str)
                  (format #f "      field_~a_slash_~a, 1, 0, 0, \"&(~a *)->~a)\",\n"
                          cident-str field-name-str cname-str field-name-str)
                  "      s7_make_signature(sc, 2,\n"
                  "        s7_make_symbol(sc, \"c-pointer?\"),\n"
                  "        s7_make_symbol(sc, \"c-pointer?\"))));\n"
                  )))
            field-names
            field-types))
        )
      (format #f "sig_~a" cident-str))))

(define (codegen port init-name decls includes protos defns inits)
  (define (fmt . args)
    (apply format port args))
  (define (for-iota proc . seqs)
    (let ((i 0))
      (apply for-each
        (lambda args
          (apply proc i args)
          (set! i (+ i 1)))
        seqs)))
  (define (for-decls proc)
    (for-each
      (lambda (kv)
        (proc (cdr kv)))
      decls))
  (define (for-cdecls-satisfying pred?)
    (lambda (proc)
      (for-decls
        (lambda (decl)
          (when (pred? decl)
            (proc decl))))))
  (define for-cprocs (for-cdecls-satisfying cproc?))
  (define for-ctypes (for-cdecls-satisfying ctype?))
  (define for-cconsts (for-cdecls-satisfying cconst?))

  ;; Includes
  (fmt "/* Includes */\n")
  (for-each fmt includes)
  (fmt "\n")

  ;; Prototypes
  (fmt "/* Prototypes */\n")
  (for-each fmt protos)
  (for-ctypes
    (lambda (ty)
      (fmt (ctype-cproto ty))))
  (fmt "\n")

  ;; Definitions
  (fmt "/* Definitions */\n")
  (for-each fmt defns)
  (for-ctypes
    (lambda (ty)
      (fmt (ctype-cdefn ty))))
  (fmt "\n")
  (for-ctypes
    (lambda (ty)
      (let ((name-str (symbol->string (ctype-name ty)))
            (cname-str (symbol->string (ctype-cname ty)))
            (cident-str (symbol->string (ctype-cident ty))))
        (fmt "static s7_pointer mset_~a(s7_scheme *sc, s7_pointer args) {\n" cident-str)
        (fmt "  s7_pointer ptr, value;\n")
        (fmt "  ptr = s7_car(args);\n")
        (fmt "  if (!s7_is_c_pointer(ptr))\n")
        (fmt "    return s7_wrong_type_error(sc, s7_make_string(sc, \"mset/~a\"), 0, ptr, c_pointer_string);\n"
             cident-str)
        (fmt "  value = s7_cadr(args);\n")
        (fmt "  if (!~a(sc, value))\n" (ctype-cpred ty))
        (fmt "    return s7_wrong_type_error(sc, s7_make_string(sc, \"mset/~a\"), 1, value, ~a);\n"
             cident-str (ctype-cerr ty))
        (fmt "  *((~a *) s7_c_pointer(ptr)) = from_~a(sc, value);\n"
             cname-str cident-str)
        (fmt "  return s7_unspecified(sc);\n")
        (fmt "}\n")

        (fmt "static s7_pointer mref_~a(s7_scheme *sc, s7_pointer args) {\n" cident-str)
        (fmt "  s7_pointer ptr;\n")
        (fmt "  ptr = s7_car(args);\n")
        (fmt "  if (!s7_is_c_pointer(ptr))\n")
        (fmt "    return s7_wrong_type_error(sc, s7_make_string(sc, \"mref/~a\"), 0, ptr, c_pointer_string);\n"
             cident-str)
        (fmt "  return into_~a(sc, *((~a *) s7_c_pointer(ptr)));\n" cident-str cname-str)
        (fmt "}\n")

        (fmt "static s7_pointer aref_~a(s7_scheme *sc, s7_pointer args) {\n" cident-str)
        (fmt "  s7_pointer ptr, index;\n")
        (fmt "  ptr = s7_car(args);\n")
        (fmt "  if (!s7_is_c_pointer(ptr))\n")
        (fmt "    return s7_wrong_type_error(sc, s7_make_string(sc, \"aref/~a\"), 0, ptr, c_pointer_string);\n"
             cident-str)
        (fmt "  index = s7_cadr(args);\n")
        (fmt "  if (!s7_is_integer(index))\n")
        (fmt "    return s7_wrong_type_error(sc, s7_make_string(sc, \"aref/~a\"), 1, index, integer_string);\n"
             cident-str)
        (fmt "  return s7_make_c_pointer(sc, ((~a *) s7_c_pointer(ptr)) + s7_integer(index));\n" cname-str)
        (fmt "}\n")
        )))
  (fmt "\n")

  ;; Wrapper procedures
  (fmt "/* Wrapper procedures */\n")
  (for-cprocs
    (lambda (pr)
      (let ((name (symbol->string (cproc-name pr)))
            (params (cproc-params pr))
            (return (cproc-return pr)))
        (fmt "static s7_pointer wrapper_~a(s7_scheme *sc, s7_pointer args) {\n" name)
        (fmt "  s7_pointer p, arg;\n")
        (for-iota
          (lambda (i p)
            (fmt "  ~a param_~a;\n" (symbol->string (ctype-cname p)) i))
          params)
        (unless (eq? return 'void)
          (fmt "  ~a retval;\n" (symbol->string (ctype-cname return))))
        (fmt "  p = args;\n")
        (fmt "\n")
        (for-iota
          (lambda (i p)
            (fmt "  arg = s7_car(p);\n")
            (fmt "  p = s7_cdr(p);\n")
            (fmt "  if (!~a(sc, arg)) {\n" (ctype-cpred p))
            (fmt "    return s7_wrong_type_error(sc,\n")
            (fmt "      s7_make_string_wrapper_with_length(sc, \"~a\", ~a), ~a, arg, ~a);\n"
                 name (string-length name) i (ctype-cerr p))
            (fmt "  }\n")
            (fmt "  param_~a = ~a(sc, arg);\n" i (ctype-s7->c p))
            (fmt "\n"))
          params)

        ;; Procedure call
        (if (eq? return 'void)
            (fmt "  ~a(" name)
            (fmt "  retval = ~a(" name))
        (for-iota
          (lambda (i p)
            (unless (= i 0)
              (fmt ", "))
            (fmt "param_~a" i))
          params)
        (fmt ");\n")

        (if (eq? return 'void)
            (fmt "  return s7_unspecified(sc);\n")
            (fmt "  return ~a(sc, retval);\n" (ctype-c->s7 return)))

        (fmt "}\n"))))
  (fmt "\n")

  ;; Initializer
  (fmt "/* Initializer */\n")
  (fmt "void ~a(s7_scheme *sc) {\n" init-name)
  (fmt "  s7_pointer curlet, sig;\n")
  (fmt "  curlet = s7_curlet(sc);\n")
  (fmt "\n")
  (for-each fmt inits)
  (for-ctypes
    (lambda (ty)
      (let ((name (symbol->string (ctype-name ty)))
            (cname (symbol->string (ctype-cname ty))))
        (fmt (ctype-cinit ty))
        (fmt "  s7_define_constant_with_documentation(sc, \"sizeof/~a\",\n" name)
        (fmt "    s7_make_integer(sc, sizeof (~a)), \"The size (in bytes) of a ~a\");\n" cname name))))
  (fmt "\n")
  (for-cconsts
    (lambda (cs)
      (fmt "  s7_define(sc, curlet, s7_make_symbol(sc, \"~a\"), ~a(sc, ~a));\n"
           (symbol->string (cconst-name cs))
           (ctype-c->s7 (cconst-type cs))
           (symbol->string (cconst-name cs)))))
  (fmt "\n")
  (for-ctypes
    (lambda (ty)
      (let ((name-str (symbol->string (ctype-name ty)))
            (cname-str (symbol->string (ctype-cname ty)))
            (cident-str (symbol->string (ctype-cident ty))))
        (fmt "  sig = s7_make_signature(sc, 3,\n")
        (fmt "    s7_make_symbol(sc, \"unspecified?\"),\n")
        (fmt "    s7_make_symbol(sc, \"c-pointer?\"),\n")
        (fmt "    s7_make_symbol(sc, \"~a?\"));\n" name-str)
        (fmt "  s7_define(sc, curlet,\n")
        (fmt "    s7_make_symbol(sc, \"mset/~a\"),\n" name-str)
        (fmt "    s7_make_typed_function(sc, \"mset/~a\", mset_~a, 2, 0, 0,\n" name-str cident-str)
        (fmt "      \"*((~a *) ptr) = value\", sig));\n" cname-str)
        (fmt "  sig = s7_make_signature(sc, 2,\n")
        (fmt "    s7_make_symbol(sc, \"~a?\"),\n" name-str)
        (fmt "    s7_make_symbol(sc, \"c-pointer?\"));\n")
        (fmt "  s7_define(sc, curlet,\n")
        (fmt "    s7_make_symbol(sc, \"mref/~a\"),\n" name-str)
        (fmt "    s7_make_typed_function(sc, \"mref/~a\", mref_~a, 1, 0, 0,\n" name-str cident-str)
        (fmt "      \"*((~a *) ptr)\", sig));\n" cname-str)
        (fmt "  sig = s7_make_signature(sc, 3,\n")
        (fmt "    s7_make_symbol(sc, \"c-pointer?\"),\n")
        (fmt "    s7_make_symbol(sc, \"c-pointer?\"),\n")
        (fmt "    s7_make_symbol(sc, \"integer?\"));\n")
        (fmt "  s7_define(sc, curlet,\n")
        (fmt "    s7_make_symbol(sc, \"aref/~a\"),\n" name-str)
        (fmt "    s7_make_typed_function(sc, \"aref/~a\", aref_~a, 2, 0, 0,\n" name-str cident-str)
        (fmt "      \"((~a *) ptr) + index\", sig));\n" cname-str)
        )))
  (fmt "\n")
  (for-cprocs
    (lambda (pr)
      (let ((name-str (symbol->string (cproc-name pr)))
            (params (cproc-params pr))
            (return (cproc-return pr)))
        (fmt "  sig = s7_make_signature(sc, ~a,\n" (+ 1 (length params)))
        (if (eq? return 'void)
            (fmt "    s7_make_symbol(sc, \"unspecified?\")")
            (fmt "    ~a(sc)" (ctype-csig return)))
        (for-iota
          (lambda (i p)
            (fmt ",\n    ~a(sc)" (ctype-csig p)))
          params)
        (fmt ");\n")

        (fmt "  s7_define(sc, curlet,\n")
        (fmt "    s7_make_symbol(sc, \"~a\"),\n" name-str)
        (fmt "    s7_make_typed_function(sc, \"~a\", wrapper_~a, ~a, 0, 0, \"~a ~a("
             name-str name-str (length params)
             (if (eq? return 'void) "void" (symbol->string (ctype-cname return)))
             name-str)
        (for-iota
          (lambda (i p)
            (unless (= i 0) (fmt ", "))
            (fmt "~a" (symbol->string (ctype-cname p))))
          params)
        (fmt ")\", sig));\n")
        )))

  (fmt "}\n\n")
  (values))

(define (gen port init-name)
  (codegen port init-name cdecls
   (reverse cincludes) (reverse cprotos) (reverse cdefns) (reverse cinits)))

#|
(define define-cstruct
  (macro (name . fields)
    (let ((tysym (gensym))
          (pred (string->symbol (append (symbol->string name) "?"))))
      `(',begin
         (',define ,name '())
         (',define ,pred '())
         (',let ((,tysym (',ctype-struct ',name ',name ',pred ',name ',name ',fields)))
           (',register-cdecl ,tysym)
           (',set! ,name (',ctype-make ,tysym))
           (',set! ,pred (',ctype-pred ,tysym))
           (,'values))))))
|#

(define (define-cconst . consts)
  (for-each
    (lambda (const)
      (let ((name (car const))
            (type (cadr const)))
        (register-cdecl
          (cconst name type))))
    consts))

(define (define-cstruct . structs)
  (for-each
    (lambda (struct)
      (let* ((name (car struct))
             (pred (string->symbol (append (symbol->string name) "?")))
             (fields (cdr struct)))
        (register-cdecl
          (ctype-struct
            name name pred name name
            fields))))
    structs))

(define (define-cenum . enums)
  (for-each
    (lambda (enum)
      (let ((name (car enum))
            (consts (cdr enum)))
        (register-cdecl (ctype-integer name name name))
        (for-each
          (lambda (const)
            (register-cdecl (cconst const name)))
          consts)))
    enums))

(define (define-cproc . procs)
  (for-each
    (lambda (proc)
      (let* ((name (car proc))
             (params (cadr proc))
             (return (caddr proc)))
        (register-cdecl
          (cproc name params return))))
    procs))

(define (cinclude path)
  (add-cinclude (format #f "#include \"~a\"\n" path)))

(define (cinclude-sys path)
  (add-cinclude (format #f "#include <~a>\n" path)))

;; Prepare the C file

(cinclude-sys "stdlib.h")
(cinclude "s7.h")
(cinclude "raylib.h")

(register-cdecl (ctype-integer 'int 'int 'int))
(register-cdecl (ctype-integer 'uint (symbol "unsigned int") (symbol "unsigned_int")))
(register-cdecl (ctype-integer 'char 'char 'char))
(register-cdecl (ctype-integer 'uchar (symbol "unsigned char") (symbol "unsigned_char")))
(register-cdecl (ctype-real 'double 'double 'double))
(register-cdecl (ctype-real 'float 'float 'float))
(register-cdecl ctype-void*)
(register-cdecl ctype-string)
(register-cdecl ctype-boolean)

;; Raylib bindings

(apply define-cenum
  `(
    (TraceLogLevel
      LOG_ALL
      LOG_TRACE
      LOG_DEBUG
      LOG_INFO
      LOG_WARNING
      LOG_ERROR
      LOG_FATAL
      LOG_NONE)
    (KeyboardKey
      KEY_A
      KEY_B
      KEY_C
      KEY_D
      KEY_P
      KEY_R
      KEY_W
      KEY_S
      KEY_U
      KEY_V
      KEY_X
      KEY_Z
      KEY_ONE
      KEY_TWO
      KEY_THREE
      KEY_FOUR
      KEY_FIVE
      KEY_SIX
      KEY_F1
      KEY_F2
      KEY_F3
      KEY_F4
      KEY_F5
      KEY_F6
      KEY_F7
      KEY_F8
      KEY_F9
      KEY_F10
      KEY_F11
      KEY_F12
      KEY_LEFT_CONTROL
      KEY_LEFT_SHIFT
      KEY_BACKSPACE
      KEY_ENTER
      KEY_UP
      KEY_DOWN
      KEY_LEFT
      KEY_RIGHT
      KEY_SPACE
      KEY_BACKSPACE
      )
    (MouseButton
      MOUSE_BUTTON_LEFT
      MOUSE_BUTTON_RIGHT
      MOUSE_BUTTON_MIDDLE)
    (Gesture
      GESTURE_NONE
      GESTURE_TAP
      GESTURE_DOUBLETAP
      GESTURE_HOLD
      GESTURE_DRAG
      GESTURE_SWIPE_RIGHT
      GESTURE_SWIPE_LEFT
      GESTURE_SWIPE_UP
      GESTURE_SWIPE_DOWN
      GESTURE_PINCH_IN
      GESTURE_PINCH_OUT)
    (ConfigFlags
      FLAG_WINDOW_RESIZABLE)
    ))

(apply define-cstruct
  `(
    (Vector2
      (x float)
      (y float))
    (Vector3
      (x float)
      (y float)
      (z float))
    (Vector4
      (x float)
      (y float)
      (z float)
      (w float))
    (Color
      (r uchar)
      (g uchar)
      (b uchar)
      (a uchar))
    (VrDeviceInfo
      (hResolution int)
      (vResolution int)
      (hScreenSize float)
      (vScreenSize float)
      (eyeToScreenDistance float)
      (lensSeparationDistance float)
      (interpupillaryDistance float)
      (lensDistortionValues (array 4 float))
      (chromaAbCorrection (array 4 float)))
    (Texture
      (id uint)
      (width int)
      (height int)
      (mipmaps int)
      (format int))
    (Rectangle
      (x float)
      (y float)
      (width float)
      (height float))
    (Camera2D
      (offset Vector2)
      (target Vector2)
      (rotation float)
      (zoom float))
    (Font
      (baseSize int)
      (glyphCount int)
      (glyphPadding int)
      (texture Texture)
      (recs void*)
      (glyphs void*))
    (AudioStream
      (buffer void*)
      (processor void*)
      (sampleRate uint)
      (sampleSize uint)
      (channels uint))
    (Sound
      (stream AudioStream)
      (frameCount uint))
    (Music
      (stream AudioStream)
      (frameCount uint)
      (looping boolean)
      (ctxType int)
      (ctxData void*))
    (Image
      (data void*)
      (width int)
      (height int)
      (mipmaps int)
      (format int))
    ))

(apply define-cconst
  `(
    (NULL void*)
    (PI double)
    (LIGHTGRAY Color)
    (GRAY Color)
    (RAYWHITE Color)
    (BROWN Color)
    (MAGENTA Color)
    (MAROON Color)
    (GOLD Color)
    (YELLOW Color)
    (PINK Color)
    (BLACK Color)
    (BLANK Color)
    (WHITE Color)
    (PURPLE Color)
    (LIME Color)
    (GREEN Color)
    ))

(apply define-cproc
  `(
    (calloc (int int) void*)
    (free (void*) void)
    (InitWindow (int int string) void)
    (SetWindowTitle (string) void)
    (SetWindowSize (int int) void)
    (CloseWindow () void)
    (WindowShouldClose () boolean)
    (SetTargetFPS (int) void)
    (GetFrameTime () float)
    (GetTime () double)
    (GetFPS () int)
    (IsWindowReady () boolean)
    (IsWindowFullscreen () boolean)
    (IsWindowHidden () boolean)
    (IsWindowMinimized () boolean)
    (IsWindowMaximized () boolean)
    (BeginDrawing () void)
    (EndDrawing () void)
    (ClearBackground (Color) void)
    (DrawText (string int int int Color) void)
    (DrawRectangle (int int int int Color) void)
    (DrawRectangleV (Vector2 Vector2 Color) void)
    (DrawRectangleLinesEx (Rectangle float Color) void)
    (DrawTriangle (Vector2 Vector2 Vector2 Color) void)
    (DrawRectangleRec (Rectangle Color) void)
    (IsKeyPressed (int) boolean)
    (IsKeyReleased (int) boolean)
    (SetGesturesEnabled (uint) void)
    (IsGestureDetected (uint) boolean)
    (TraceLog (int string) void)
    (SetTraceLogLevel (int) void)
    (DrawFPS (int int) void)
    (GetMousePosition () Vector2)
    (GetMouseX () int)
    (GetMouseY () int)
    (IsMouseButtonPressed (int) boolean)
    (IsMouseButtonDown (int) boolean)
    (IsMouseButtonReleased (int) boolean)
    (DrawCircle (int int float Color) void)
    (DrawCircleV (Vector2 float Color) void)
    (DrawPixel (int int Color) void)
    (DrawLine (int int int int Color) void)
    (DrawLineV (Vector2 Vector2 Color) void)
    (DrawLineEx (Vector2 Vector2 float Color) void)
    (LoadTexture (string) Texture)
    (UnloadTexture (Texture) void)
    (DrawTexture (Texture int int Color) void)
    (DrawTexturePro (Texture Rectangle Rectangle Vector2 float Color) void)
    (BeginMode2D (Camera2D) void)
    (EndMode2D () void)
    (GetWorldToScreen2D (Vector2 Camera2D) Vector2)
    (GetScreenToWorld2D (Vector2 Camera2D) Vector2)
    (ExportDataAsCode (void* int string) boolean)
    (DrawLineStrip (void* int Color) void)
    (GetKeyPressed () int)
    (GetCharPressed () int)
    (LoadFont (string) Font)
    (LoadFontEx (string int void* int) Font)
    (UnloadFont (Font) void)
    (DrawTextEx (Font string Vector2 float float Color) void)
    (DrawTextPro (Font string Vector2 Vector2 float float float Color) void)
    (InitAudioDevice () void)
    (CloseAudioDevice () void)
    (LoadSound (string) Sound)
    (LoadMusicStream (string) Music)
    (PlaySound (Sound) void)
    (ToggleFullscreen () void)
    (GetTouchX () int)
    (GetTouchY () int)
    (GetGestureDragVector () Vector2)
    (PlayMusicStream (Music) void)
    (UpdateMusicStream (Music) void)
    (SetMusicVolume (Music float) void)
    (SeekMusicStream (Music float) void)
    (GetMusicTimePlayed (Music) float)
    (LoadImage (string) Image)
    (SetWindowIcon (Image) void)
    (GetScreenWidth () int)
    (GetScreenHeight () int)
    (SetWindowState (uint) void)
    (GetFontDefault () Font)
    ))

;; Write the C file to stdout

(catch #t
  (lambda ()
    (display (with-output-to-string
               (lambda () (gen #t 'rlbind)))))
  (lambda args
    (unless (null? (cadr args))
      (apply format *stderr* (cadr args))
      (newline *stderr*))
    (exit 1)))

()

