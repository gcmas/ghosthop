# About

This is a small puzzle game made for the 2024 Spring Lisp Game Jam.
The objective is to reach the goal by assigning rules to colors.

All gameplay code and assets (i.e., everything in `./scm` and `./asset`) were made during the jam.

The `Makefile`, `rlbind.scm`, and C code (i.e., everything in `./src`) were created as general-purpose tools
before the start of the jam. `rlbind.scm` is a Scheme script that auto-generates C code that creates
Raylib bindings to s7 Scheme.

# Credit

The music is ["Lines of Code"](https://opengameart.org/content/lines-of-code)
by Trevor Lentz, licensed under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)

Gameplay code and all other assets were made by Mason Cutting (gcmas@proton.me).

The following tools and libraries were used:
- [s7 Scheme](https://ccrma.stanford.edu/software/snd/snd/s7.html): Scheme interpreter embedded in C
- [Raylib](https://www.raylib.com/): cross-platform game library
- [Piskel](https://www.piskelapp.com/): sprite creation
- [jsfxr](https://sfxr.me/): sound effects

# Controls

- Move up/left/down/right with `w`/`a`/`s`/`d` keys, the up/left/down/right arrow keys,
  or by swiping up/left/down/right.
- Confirm or start the next level with `z`/`Enter` keys or by clicking the "play" button on screen.
- Cancel or exit the level with `x`/`Backspace` keys or by clicking the "x" or "home" button on screen.
- Undo an action by with `c`/`u` keys or by clicking the "undo" button on screen.
- Restart the current level with `v`/`r` keys or by clicking the "rewind" button on screen.

# Building

This project depends on
[`s7 Scheme`](https://cm-gitlab.stanford.edu/bil/s7/-/commit/3c5ad3901f06bdde62ca99f237237f910e2ad33e)
and [`Raylib`](https://github.com/raysan5/raylib/commit/02d98a3e44b13584c98a7aaee0ca6fc5fb55a975)
(located in `./vendor/`).
Before building, clone them with `git submodule init && git submodule update`.

To build from source on Linux, run `make`/`make all` in the project root directory (use `-j<n>` flag for faster builds).
Use `make linux` for the Linux build, `make windows` to cross-compile for Windows, or `make wasm` for WebAssembly.

## Dependencies
- [TCC](https://www.bellard.org/tcc/) is used in some cases for faster build times.
  If it is unavailable, run e.g. `make TCC=gcc` to use GCC instead.
- For cross-compilation to Windows, [MinGW-w64](https://www.mingw-w64.org/downloads/) is required.
- For the WebAssembly build, [Emscripten](https://emscripten.org/) is required.

